import bpy
from bpy.types import Operator, AddonPreferences
from bpy.props import BoolProperty

class DieselAnimationPreferences(AddonPreferences):
	bl_idname = __package__

	importDebugOutput = BoolProperty(
		name="Import Debug Output",
		default=False
	)
	exportDebugOutput = BoolProperty(
		name="Export Debug Output",
		default=False
	)

	def draw(self, context):
		layout = self.layout
		layout.prop(self, "importDebugOutput")
		layout.prop(self, "exportDebugOutput")