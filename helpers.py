import bpy, operator
blast_to_the_past = (bpy.app.version < (2, 80))
trapped_behind_293 = (bpy.app.version < (2, 93))

# Convert class fields to annotations to make Blender 2.8, and now 2.93 chill.
def make_annotations(cls):
	if blast_to_the_past:
		return cls

	bl_props = None
	if trapped_behind_293:
		bl_props = {k: v for k, v in cls.__dict__.items() if isinstance(v, tuple)}
	else:
		bl_props = {k: v for k, v in cls.__dict__.items() if isinstance(v, bpy.props._PropertyDeferred)}

	if bl_props:
		if '__annotations__' not in cls.__dict__:
			setattr(cls, '__annotations__', {})
		annotations = cls.__dict__['__annotations__']
		for k, v in bl_props.items():
			annotations[k] = v
			delattr(cls, k)
	return cls

# To account for property change from 2.7->2.8
def get_property(propertyName):
	if blast_to_the_past:
		preferences = bpy.context.user_preferences.addons.get(__package__).preferences
	else:
		preferences = bpy.context.preferences.addons.get(__package__).preferences

	if propertyName in preferences:
		return preferences[propertyName]
	else:
		return None

# To account for the matrix multiplication change from 2.7->2.8
def matrix_mult(a, b):
	if blast_to_the_past:
		return a * b
	else:
		return operator.matmul(a, b)