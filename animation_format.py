debug_prints_enabled = False
def debug_print(*args):
	if debug_prints_enabled:
		print(*args)

import zlib
import struct
import math

class DieselAnimationObject():
	name = ""
	positions = None
	rotations = None

	def __init__(self, name):
		self.name = name
		self.positions = {}
		self.rotations = {}

	def add_position(self, time, x, y, z, stepped=False):
		self.positions[time] = DieselAnimationFrame((x, y, z), stepped)

	def add_rotation(self, time, x, y, z, w, stepped=False):
		self.rotations[time] = DieselAnimationFrame((x, y, z, w), stepped)

class DieselAnimationFrame():
	data = None
	stepped = False

	def __init__(self, data, stepped=False):
		self.data = data
		self.stepped = stepped

class DieselAnimationTrigger():
	time = 0.0
	name = ""

	def __init__(self, time, name):
		self.time = time
		self.name = name

################
# Data Structs #
################
class DieselAnimationStructs():

	# Header Structs
	header_32bit = struct.Struct("<IxxxxxxxxIfIIIIIIIIII")
	header_64bit = struct.Struct("<QxxxxxxxxQxxxxxxxxfxxxxQQQQQQQQQQ")

	# Object Name Header Structs
	name_header_32bit = struct.Struct("<I")
	name_header_64bit = struct.Struct("<Q")

	# Trigger Header Structs
	trigger_header_32bit = struct.Struct("<fI")
	trigger_header_64bit = struct.Struct("<fxxxxQ")

	# Position Header Structs
	positions_header1_32bit = struct.Struct("<II")
	positions_header2_32bit = struct.Struct("<xxxxII")
	positions_header1_64bit = struct.Struct("<QQ")
	positions_header2_64bit = struct.Struct("<xxxxxxxxQQ")

	# Position Structs
	positions = {}
	positions[0x9D3DE40D] = struct.Struct("<ffff")
	positions[0x1196CFB2] = struct.Struct("<ffffI")
	positions[0x1DC686E0] = struct.Struct("<fHHHxx")

	# Rotation Header Structs
	rotations_header1_32bit = struct.Struct("<II")
	rotations_header2_32bit = struct.Struct("<xxxxII")
	rotations_header1_64bit = struct.Struct("<QQ")
	rotations_header2_64bit = struct.Struct("<xxxxxxxxQQ")

	# Rotation Structs
	rotations = {}
	rotations[0x9DFB92B6] = struct.Struct("<fffff")
	rotations[0xE91A69BA] = struct.Struct("<fffffI")
	rotations[0x96ECB85C] = struct.Struct("<fI")

	# Extras
	zlib_full_file_size = struct.Struct("<I")

class DieselAnimation():
	length = 0
	objects = None
	triggers = None

	def __init__(self):
		self.length = 0
		self.objects = []
		self.triggers = []

	def set_length(self, length):
		self.length = length

	def add_object(self, name):
		new_object = DieselAnimationObject(name)
		self.objects.append(new_object)
		return new_object

	def add_trigger(self, time, name):
		new_trigger = DieselAnimationTrigger(time, name)
		self.triggers.append(new_trigger)
		return new_trigger

	def __read_header(self, animation_data, is_64bit):
		header_unpacker = DieselAnimationStructs.header_32bit
		if is_64bit:
			header_unpacker = DieselAnimationStructs.header_64bit

		header_data = header_unpacker.unpack(animation_data[0:header_unpacker.size])

		return header_data

	def __read_data(self, header_data_tuple, animation_data, is_64bit):
		##############################
		# 32 Bit or 64 Bit Unpackers #
		##############################

		object_names_header_unpacker = DieselAnimationStructs.name_header_32bit
		triggers_header_unpacker = DieselAnimationStructs.trigger_header_32bit

		positions_header1_unpacker = DieselAnimationStructs.positions_header1_32bit
		positions_header2_unpacker = DieselAnimationStructs.positions_header2_32bit

		rotations_header1_unpacker = DieselAnimationStructs.rotations_header1_32bit
		rotations_header2_unpacker = DieselAnimationStructs.rotations_header2_32bit

		if is_64bit:
			object_names_header_unpacker = DieselAnimationStructs.name_header_64bit
			triggers_header_unpacker = DieselAnimationStructs.trigger_header_64bit

			positions_header1_unpacker = DieselAnimationStructs.positions_header1_64bit
			positions_header2_unpacker = DieselAnimationStructs.positions_header2_64bit

			rotations_header1_unpacker = DieselAnimationStructs.rotations_header1_64bit
			rotations_header2_unpacker = DieselAnimationStructs.rotations_header2_64bit

		##########
		# HEADER #
		##########

		debug_print("\nHeader Data:")

		# Basic Header Stuff
		file_id = header_data_tuple[0]
		debug_print("\tFile ID: {}".format(file_id))

		file_size = header_data_tuple[1]
		debug_print("\tFile Size: {}".format(file_size))

		animation_length = header_data_tuple[2]
		debug_print("\tAnimation Length: {}".format(animation_length))

		# Object Names
		number_of_object_names = header_data_tuple[3]
		offset_of_object_names = header_data_tuple[4]
		debug_print("\tNumber of Object Names: {} - Offset: {}".format(number_of_object_names, offset_of_object_names))

		# Unknown Things
		number_of_unknown_things = header_data_tuple[5]
		offset_of_unknown_things = header_data_tuple[6]
		debug_print("\tNumber of Unknown Things: {} - Offset: {}".format(number_of_unknown_things, offset_of_unknown_things))

		# Triggers
		number_of_triggers = header_data_tuple[7]
		offset_of_triggers = header_data_tuple[8]
		debug_print("\tNumber of Triggers: {} - Offset: {}".format(number_of_triggers, offset_of_triggers))

		# Object Positions
		number_of_object_positions = header_data_tuple[9]
		offset_of_object_positions = header_data_tuple[10]
		debug_print("\tNumber of Object Positions: {} - Offset: {}".format(number_of_object_positions, offset_of_object_positions))

		# Object Rotations
		number_of_object_rotations = header_data_tuple[11]
		offset_of_object_rotations = header_data_tuple[12]
		debug_print("\tNumber of Object Rotations: {} - Offset: {}".format(number_of_object_positions, offset_of_object_rotations))

		self.set_length(animation_length)

		###################
		# SECTION READING #
		###################

		# Null terminated string reader.
		def read_string(offset):
			temp_string = ""
			for byte in animation_data[offset:]:
				if byte == 0:
					return temp_string

				temp_string += chr(byte)

		################
		# Object Names #
		################

		debug_print("\nObjects:")

		object_names_header_size = number_of_object_names * object_names_header_unpacker.size
		object_names_header_data_iterator = object_names_header_unpacker.iter_unpack(animation_data[offset_of_object_names:offset_of_object_names+object_names_header_size])

		for object_names_header_data in object_names_header_data_iterator:
			name_offset = object_names_header_data[0]
			name = read_string(name_offset)

			self.add_object(name)

			debug_print("\t{}".format(name))

		############
		# Triggers #
		############

		debug_print("\nTriggers:")

		triggers_headers_size = number_of_triggers * triggers_header_unpacker.size
		triggers_header_data_iterator = triggers_header_unpacker.iter_unpack(animation_data[offset_of_triggers:offset_of_triggers+triggers_headers_size])

		for triggers_header_data in triggers_header_data_iterator:
			trigger_time = self.length * triggers_header_data[0]
			trigger_name_offset = triggers_header_data[1]
			trigger_name = read_string(trigger_name_offset)

			self.add_trigger(trigger_time, trigger_name)
			debug_print("\t{:<24} - {}".format(trigger_time, trigger_name))

		####################
		# Object Positions #
		####################

		debug_print("\nObject Positions:")

		object_positions_headers_size = number_of_object_positions * positions_header1_unpacker.size
		object_positions_header_data_iterator = positions_header1_unpacker.iter_unpack(animation_data[offset_of_object_positions:offset_of_object_positions+object_positions_headers_size])

		for i, object_positions_header_data in enumerate(object_positions_header_data_iterator):
			position_type = object_positions_header_data[0]
			positions_offset = object_positions_header_data[1]

			relevant_object = self.objects[i]
			debug_print("\t{:<32} - (Position Type: 0x{:X}):".format(relevant_object.name, position_type))

			position_header = positions_header2_unpacker.unpack(animation_data[positions_offset:positions_offset+positions_header2_unpacker.size])

			number_of_positions = position_header[0]
			offset_of_positions = position_header[1]

			if position_type in DieselAnimationStructs.positions:
				position_unpacker = DieselAnimationStructs.positions[position_type]
				position_data_size = number_of_positions * position_unpacker.size
				position_iterator = position_unpacker.iter_unpack(animation_data[offset_of_positions:offset_of_positions+position_data_size])

				if position_type == 0x9D3DE40D:
					for position_data in position_iterator:
						time = position_data[0]
						x = position_data[1]
						y = position_data[2]
						z = position_data[3]

						relevant_object.add_position(time, x, y, z)
						debug_print("\t\t{:<24} - ( X:{:<24} Y:{:<24} Z:{:<24} )".format(time, x, y, z))

				elif position_type == 0x1196CFB2:
					for position_data in position_iterator:
						time = position_data[0]
						x = position_data[1]
						y = position_data[2]
						z = position_data[3]
						stepped = position_data[4]

						stepped_string = ""
						if stepped > 0:
							stepped_string = " - (Stepped Interpolation)"

						relevant_object.add_position(time, x, y, z, stepped > 0)

						debug_print("\t\t{:<24} - ( X:{:<24} Y:{:<24} Z:{:<24} ){}".format(time, x, y, z, stepped_string))

				elif position_type == 0x1DC686E0:
					for position_data in position_iterator:
						time = position_data[0]
						x = ((position_data[1] / 65535) * 2000) - 1000
						y = ((position_data[2] / 65535) * 2000) - 1000
						z = ((position_data[3] / 65535) * 2000) - 1000

						relevant_object.add_position(time, x, y, z)
						debug_print("\t\t{:<24} - ( X:{:<24} Y:{:<24} Z:{:<24} )".format(time, x, y, z))

			else:
				debug_print("\t\tPosition Type Unsupported")

		####################
		# Object Rotations #
		####################

		debug_print("\nObject Rotations:")

		object_rotations_headers_size = number_of_object_rotations * rotations_header1_unpacker.size
		object_rotations_header_data_iterator = rotations_header1_unpacker.iter_unpack(animation_data[offset_of_object_rotations:offset_of_object_rotations+object_rotations_headers_size])

		for i, object_rotations_header_data in enumerate(object_rotations_header_data_iterator):
			rotation_type = object_rotations_header_data[0]
			rotations_offset = object_rotations_header_data[1]

			relevant_object = self.objects[i]
			debug_print("\t{:<32} - (Rotation Type: 0x{:X}):".format(relevant_object.name, rotation_type))

			rotation_header = rotations_header2_unpacker.unpack(animation_data[rotations_offset:rotations_offset+rotations_header2_unpacker.size])

			number_of_rotations = rotation_header[0]
			offset_of_rotations = rotation_header[1]

			if rotation_type in DieselAnimationStructs.rotations:
				rotation_unpacker = DieselAnimationStructs.rotations[rotation_type]
				rotation_data_size = number_of_rotations * rotation_unpacker.size
				rotation_iterator = rotation_unpacker.iter_unpack(animation_data[offset_of_rotations:offset_of_rotations+rotation_data_size])

				if rotation_type == 0x9DFB92B6:
					for rotation_data in rotation_iterator:
						time = rotation_data[0]
						x = rotation_data[1]
						y = rotation_data[2]
						z = rotation_data[3]
						w = rotation_data[4]

						relevant_object.add_rotation(time, x, y, z, w)
						debug_print("\t\t{:<24} - ( W:{:<24} X:{:<24} Y:{:<24} Z:{:<24} )".format(time, w, x, y, z))

				elif rotation_type == 0xE91A69BA:
					for rotation_data in rotation_iterator:
						time = rotation_data[0]
						x = rotation_data[1]
						y = rotation_data[2]
						z = rotation_data[3]
						w = rotation_data[4]
						stepped = rotation_data[5]

						stepped_string = ""
						if stepped > 0:
							stepped_string = " - (Stepped Interpolation)"
						else:
							relevant_object.add_rotation(time, x, y, z, w)

						debug_print("\t\t{:<24} - ( W:{:<24} X:{:<24} Y:{:<24} Z:{:<24} ){}".format(time, w, x, y, z, stepped_string))

				elif rotation_type == 0x96ECB85C:
					for rotation_data in rotation_iterator:
						time = rotation_data[0]
						data = rotation_data[1]

						li_mask = 0b011
						mask = 0b01111111111

						largest_index = data & li_mask

						lower_range = -0.75
						range_size = 1.5

						first_value = ((data >> 2) & mask)
						second_value = ((data >> 12) & mask)
						third_value = ((data >> 22) & mask)

						first_value = first_value * (range_size / 1023) + lower_range
						second_value = second_value * (range_size / 1023) + lower_range
						third_value = third_value * (range_size / 1023) + lower_range

						largest_value = math.sqrt(1 - (first_value*first_value) - (second_value*second_value) - (third_value*third_value))

						x, y, z, w = (0,0,0,0)

						if largest_index == 0:
							x, y, z, w = (largest_value, first_value, second_value, third_value)
						elif largest_index == 1:
							x, y, z, w = (third_value, largest_value, first_value, second_value)
						elif largest_index == 2:
							x, y, z, w = (second_value, third_value, largest_value, first_value)
						elif largest_index == 3:
							x, y, z, w = (first_value, second_value, third_value, largest_value)

						relevant_object.add_rotation(time, x, y, z, w)

						debug_print("\t\t{:<24} - ( W:{:<24} X:{:<24} Y:{:<24} Z:{:<24} )".format(time, w, x, y, z))

			else:
				debug_print("\t\tRotation Type Unsupported")

	def read(self, filepath):
		debug_print("\nReading Diesel Animation: {}".format(filepath))

		######################
		# ZLib Decompression #
		######################

		animation_file = open(filepath, 'rb')
		animation_data = zlib.decompress(animation_file.read())
		animation_file.close()

		##################
		# Read File Data #
		##################
		is_64bit = False

		if len(animation_data) < 60:
			debug_print("\nCompletely Invalid Animation File! Less than 60 bytes!")
			return

		header_data_tuple = self.__read_header(animation_data, False)

		# Check if header is valid as a 64 bit check.
		header_file_size = header_data_tuple[1]
		actual_file_size = len(animation_data)

		# If they don't match read 64 bit header and check if that is valid.
		if header_file_size != actual_file_size:
			debug_print("\n32 Bit Failure")
			debug_print("Header File Size:{}".format(header_file_size))
			debug_print("Actual File Size:{}".format(actual_file_size))

			if len(animation_data) < 120:
				debug_print("\nCompletely Invalid Animation File! Less than 120 bytes!")
				return

			header_data_tuple = self.__read_header(animation_data, True)

			# Guess if header is valid for that 64 bit check.
			header_file_size = header_data_tuple[1]
			actual_file_size = len(animation_data)

			if header_file_size != actual_file_size:
				debug_print("\nCompletely Invalid Animation File! File Size Incorrect")
				debug_print("Header File Size:{}".format(header_file_size))
				debug_print("Actual File Size:{}".format(actual_file_size))
				return

			is_64bit = True

		# Now read the data.
		self.__read_data(header_data_tuple, animation_data, is_64bit)		

	def write(self, filepath, is_64bit=False, position_type=0x1DC686E0, rotation_type=0x9DFB92B6):
		debug_print("\nWriting Diesel Animation: {}".format(filepath))

		############################
		# 32 Bit or 64 Bit Packers #
		############################
		header_packer = DieselAnimationStructs.header_32bit

		object_names_header_packer = DieselAnimationStructs.name_header_32bit
		triggers_header_packer = DieselAnimationStructs.trigger_header_32bit

		positions_header1_packer = DieselAnimationStructs.positions_header1_32bit
		positions_header2_packer = DieselAnimationStructs.positions_header2_32bit

		rotations_header1_packer = DieselAnimationStructs.rotations_header1_32bit
		rotations_header2_packer = DieselAnimationStructs.rotations_header2_32bit

		if is_64bit:
			header_packer = DieselAnimationStructs.header_64bit

			object_names_header_packer = DieselAnimationStructs.name_header_64bit
			triggers_header_packer = DieselAnimationStructs.trigger_header_64bit

			positions_header1_packer = DieselAnimationStructs.positions_header1_64bit
			positions_header2_packer = DieselAnimationStructs.positions_header2_64bit

			rotations_header1_packer = DieselAnimationStructs.rotations_header1_64bit
			rotations_header2_packer = DieselAnimationStructs.rotations_header2_64bit

		#################
		# Section Sizes #
		#################
		position_type_packer = DieselAnimationStructs.positions[position_type]
		rotation_type_packer = DieselAnimationStructs.rotations[rotation_type]

		#################
		# Section Sizes #
		#################
		debug_print("\nSection Sizes:")

		header_size = header_packer.size

		unknowns_count = 0
		unknowns_size = 0

		triggers_count = len(self.triggers)
		triggers_size = triggers_count * triggers_header_packer.size
		for trigger in self.triggers:
			triggers_size += len(trigger.name) + 1

		object_count = len(self.objects)

		object_names_size = object_count * object_names_header_packer.size
		object_positions_size = object_count * (positions_header1_packer.size + positions_header2_packer.size)
		object_rotations_size = object_count * (rotations_header1_packer.size + rotations_header2_packer.size)
		for diesel_object in self.objects:
			object_names_size += len(diesel_object.name) + 1
			object_positions_size += len(diesel_object.positions) * position_type_packer.size
			object_rotations_size += len(diesel_object.rotations) * rotation_type_packer.size

		total_size = header_size + object_names_size + unknowns_size + triggers_size + object_positions_size + object_rotations_size

		debug_print("\tHeader Size: {}".format(header_size))
		debug_print("\tObject Names Size: {}".format(object_names_size))
		debug_print("\tUnknowns Size: {}".format(unknowns_size))
		debug_print("\tTriggers Size: {}".format(triggers_size))
		debug_print("\tObject Positions Size: {}".format(object_positions_size))
		debug_print("\tObject Rotations Size: {}".format(object_rotations_size))

		debug_print("\n\tTotal Size: {}".format(total_size))

		###################
		# Section Offsets #
		###################
		debug_print("\nSection Offsets:")

		object_names_offset = header_size
		unknowns_offset = object_names_offset + object_names_size
		triggers_offset = unknowns_offset + unknowns_size
		object_positions_offset = triggers_offset + triggers_size
		object_rotations_offset = object_positions_offset + object_positions_size

		debug_print("\tObject Names Offset: {}".format(object_names_offset))
		debug_print("\tUnknowns Offset: {}".format(unknowns_offset))
		debug_print("\tTriggers Offset: {}".format(triggers_offset))
		debug_print("\tObject Positions Offset: {}".format(object_positions_offset))
		debug_print("\tObject Rotations Offset: {}".format(object_rotations_offset))

		animation_data = bytearray(total_size)

		####################
		# Pack Header Data #
		####################
		header_packer.pack_into(animation_data, 0,
			142855301,
			total_size,
			self.length,
			object_count,
			object_names_offset,
			unknowns_count,
			unknowns_offset,
			triggers_count,
			triggers_offset,
			object_count,
			object_positions_offset,
			object_count,
			object_rotations_offset
		)

		###################
		# Section Writing #
		###################

		# Null terminated string writer.
		def write_strings(offset, strings):
			current_index = offset

			for string in strings:
				for character in string:
					animation_data[current_index] = ord(character)
					current_index += 1
				animation_data[current_index] = 0
				current_index += 1

		#####################
		# Pack Object Names #
		#####################
		debug_print("\nPacking Object Names:")

		object_name_strings_to_write = []
		object_name_string_offset = object_names_offset + (object_count * object_names_header_packer.size)
		current_object_name_string_offset = object_name_string_offset

		for index, diesel_object in enumerate(self.objects):
			debug_print("\t{}".format(diesel_object.name))

			object_names_header_packer.pack_into(animation_data, object_names_offset + (index * object_names_header_packer.size),
				current_object_name_string_offset
			)

			object_name_strings_to_write.append(diesel_object.name)
			current_object_name_string_offset += len(diesel_object.name) + 1

		write_strings(object_name_string_offset, object_name_strings_to_write)

		#################
		# Pack Triggers #
		#################
		debug_print("\nPacking Triggers:")

		sorted_triggers = sorted(self.triggers, key=lambda trigger: trigger.time)

		trigger_strings_to_write = []
		trigger_string_offset = triggers_offset + (triggers_count * triggers_header_packer.size)
		current_trigger_string_offset = trigger_string_offset

		for index, trigger in enumerate(sorted_triggers):
			debug_print("\t{:<24} - {}".format(trigger.time, trigger.name))

			triggers_header_packer.pack_into(animation_data, triggers_offset + (index * triggers_header_packer.size),
				trigger.time / self.length,
				current_trigger_string_offset
			)

			trigger_strings_to_write.append(trigger.name)
			current_trigger_string_offset += len(trigger.name) + 1

		write_strings(trigger_string_offset, trigger_strings_to_write)

		##################
		# Pack Positions #
		##################
		debug_print("\nPacking Positions:")

		position_data_to_write = []
		position_data_offset = object_positions_offset + (object_count * positions_header1_packer.size)
		current_position_data_offset = position_data_offset

		for index, diesel_object in enumerate(self.objects):
			debug_print("\t{}".format(diesel_object.name))

			positions_header1_packer.pack_into(animation_data, object_positions_offset + (index * positions_header1_packer.size),
				position_type,
				current_position_data_offset
			)

			position_data_to_write.append(diesel_object.positions)
			current_position_data_offset += positions_header2_packer.size + (len(diesel_object.positions) * position_type_packer.size)

		current_position_data_offset = position_data_offset
		for position_data in position_data_to_write:
			positions_header2_packer.pack_into(animation_data, current_position_data_offset,
				len(position_data),
				current_position_data_offset + positions_header2_packer.size
			)
			current_position_data_offset += positions_header2_packer.size

			if position_type == 0x9D3DE40D:
				for time, frame in sorted(position_data.items()):
					x = frame.data[0]
					y = frame.data[1]
					z = frame.data[2]

					position_type_packer.pack_into(animation_data, current_position_data_offset,
						time,
						x,
						y,
						z
					)

					current_position_data_offset += position_type_packer.size

			elif position_type == 0x1196CFB2:
				for time, frame in sorted(position_data.items()):
					x = frame.data[0]
					y = frame.data[1]
					z = frame.data[2]

					position_type_packer.pack_into(animation_data, current_position_data_offset,
						time,
						x,
						y,
						z,
						int(frame.stepped)
					)

					current_position_data_offset += position_type_packer.size

			elif position_type == 0x1DC686E0:
				for time, frame in sorted(position_data.items()):
					x = round(((frame.data[0] + 1000) / 2000) * 65535)
					y = round(((frame.data[1] + 1000) / 2000) * 65535)
					z = round(((frame.data[2] + 1000) / 2000) * 65535)

					position_type_packer.pack_into(animation_data, current_position_data_offset,
						time,
						x,
						y,
						z
					)

					current_position_data_offset += position_type_packer.size

		##################
		# Pack Rotations #
		##################
		debug_print("\nPacking Rotations:")

		rotation_data_to_write = []
		rotation_data_offset = object_rotations_offset + (object_count * rotations_header1_packer.size)
		current_rotation_data_offset = rotation_data_offset

		for index, diesel_object in enumerate(self.objects):
			debug_print("\t{}".format(diesel_object.name))

			rotations_header1_packer.pack_into(animation_data, object_rotations_offset + (index * rotations_header1_packer.size),
				rotation_type,
				current_rotation_data_offset
			)

			rotation_data_to_write.append(diesel_object.rotations)
			current_rotation_data_offset += rotations_header2_packer.size + (len(diesel_object.rotations) * rotation_type_packer.size)

		current_rotation_data_offset = rotation_data_offset
		for rotation_data in rotation_data_to_write:
			rotations_header2_packer.pack_into(animation_data, current_rotation_data_offset,
				len(rotation_data),
				current_rotation_data_offset + rotations_header2_packer.size
			)
			current_rotation_data_offset += rotations_header2_packer.size

			if rotation_type == 0x9DFB92B6:
				for time, frame in sorted(rotation_data.items()):
					x = frame.data[0]
					y = frame.data[1]
					z = frame.data[2]
					w = frame.data[3]

					rotation_type_packer.pack_into(animation_data, current_rotation_data_offset,
						time,
						x,
						y,
						z,
						w
					)

					current_rotation_data_offset += rotation_type_packer.size

			elif rotation_type == 0xE91A69BA:
				for time, frame in sorted(rotation_data.items()):
					x = frame.data[0]
					y = frame.data[1]
					z = frame.data[2]
					w = frame.data[3]

					rotation_type_packer.pack_into(animation_data, current_rotation_data_offset,
						time,
						x,
						y,
						z,
						w,
						int(frame.stepped)
					)

					current_rotation_data_offset += rotation_type_packer.size

			elif rotation_type == 0x96ECB85C:
				for time, frame in sorted(rotation_data.items()):
					x = frame.data[0]
					y = frame.data[1]
					z = frame.data[2]
					w = frame.data[3]

					largest_value = max(frame.data, key=abs)
					largest_index = frame.data.index(largest_value)

					if largest_value < 0:
						x = -x;
						y = -y;
						z = -z;
						w = -w;

					lower_range = -0.75
					range_size = 1.5

					first_value = 0
					second_value = 0
					third_value = 0

					first_value, second_value, third_value = (0,0,0)

					if largest_index == 0:
						first_value, second_value, third_value = (y, z, w)
					elif largest_index == 1:
						first_value, second_value, third_value = (z, w, x)
					elif largest_index == 2:
						first_value, second_value, third_value = (w, x, y)
					elif largest_index == 3:
						first_value, second_value, third_value = (x, y, z)

					first_value = (first_value - lower_range) * 1023 / range_size
					second_value = (second_value - lower_range) * 1023 / range_size
					third_value = (third_value - lower_range) * 1023 / range_size

					first_value = round(first_value) << 2
					second_value = round(second_value) << 12
					third_value = round(third_value) << 22

					data = largest_index + first_value + second_value + third_value

					rotation_type_packer.pack_into(animation_data, current_rotation_data_offset,
						time,
						data
					)

					current_rotation_data_offset += rotation_type_packer.size

		animation_file = open(filepath, 'wb')
		animation_file.write(zlib.compress(animation_data) + DieselAnimationStructs.zlib_full_file_size.pack(total_size))
		animation_file.close()