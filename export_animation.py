debug_prints_enabled = False
def debug_print(*args):
	if debug_prints_enabled:
		print(*args)

from . import helpers, animation_format

import bpy
from bpy.props import (IntProperty, StringProperty, BoolProperty, EnumProperty)
from mathutils import (Vector, Quaternion, Matrix)
from bpy_extras.io_utils import (ExportHelper)

import zlib
import struct
import math

class DieselAnimationExporter(bpy.types.Operator, ExportHelper):
	bl_idname = "export_scene.animation"
	bl_label = "Export Diesel Animation"
	bl_description = "Export a Diesel Animation"

	filename_ext = ".animation"
	filter_glob = StringProperty(default = "*.animation", options = {'HIDDEN'})

	position_type = EnumProperty(
		items=(
			("0x9D3DE40D", "32-Bit Floats", "Vector 3 stored with 32-Bit floats used for X, Y, Z. This format is found in a few first person animations."),
			("0x1196CFB2", "32-Bit Floats (w/ Stepped Interpolation Flag - Not Implemented Yet)", "Identical to '32-Bit Floats', but contains a a stepped interpolation flag. This format is usually found in heist props/vehicles."),
			("0x1DC686E0", "Quaternized Compressed (-10m to 10m)", "Vector 3 stored with quaternized X, Y, and Z values. This limits positions to -10m to 10m from their parent. This format is usually found in first person animations.")
		),
		name="Position Type",
		description="The position type to use in the exported .animation file."
	)

	rotation_type = EnumProperty(
		items=(
			("0x9DFB92B6", "32-Bit Floats", "Standard quaternion with 32-Bit floats used for X, Y, Z, and W. This format is usually found in first person animations."),
			("0xE91A69BA", "32-Bit Floats (w/ Stepped Interpolation Flag - Not Implemented Yet)", "Identical to '32-Bit Floats', but contains a a stepped interpolation flag. This format is usually found in heist props/vehicles."),
			("0x96ECB85C", "10-Bit Compressed", "Compressed quaternion with 10-bit quaternized values used to store the three smallest values, whilst the largest is discarded and re-generated later. This format is usually found in third person animations.")
		),
		name="Rotation Type",
		description="The rotation type to use in the exported .animation file."
	)

	export_detailed = BoolProperty(
		name="Export Detailed",
		description="Export data for every frame on objects with more than one keyframe."
	)

	export_triggers = BoolProperty(
		name="Export Triggers",
		default=True
	)

	export_64bit = BoolProperty(
		name="Export 64-Bit",
		description="Export 64-Bit animation for use in 64-Bit diesel games, like RAID WW2."
	)

	framerate = IntProperty(
		name="Frame Rate",
		description="The frame rate of the animation.",
		default=30,
		min=0,
		max=2400
	)

	only_selected = BoolProperty(
		name="Only Export Selected",
		default=False
	)

	def execute(self, context):
		animation_export(
			self.filepath,
			int(self.properties.position_type, 16),
			int(self.properties.rotation_type, 16),
			self.properties.export_detailed,
			self.properties.export_triggers,
			self.properties.export_64bit,
			self.properties.framerate,
			self.properties.only_selected,
			helpers.get_property("exportDebugOutput")
		)

		return {'FINISHED'}

	def invoke(self, context, event):
		bpy.context.window_manager.fileselect_add(self)

		return {'RUNNING_MODAL'}

def animation_export(filepath, position_type=499549920, rotation_type=2650510006, export_detailed=False, export_triggers=True, export_64bit=False, framerate=30, only_selected=False, debug_output=False):
	if debug_output:
		debug_prints_enabled = True
		animation_format.debug_prints_enabled = True

	diesel_animation = animation_format.DieselAnimation()

	###########
	# OBJECTS #
	###########

	scene = bpy.context.scene
	blender_scale = scene.unit_settings.scale_length

	frame_length = scene.frame_end - scene.frame_start
	diesel_animation.set_length(frame_length / framerate)

	#sorting method
	def keyfunc(entry):
		return entry[0]

	#generic set frame function
	def set_frame(frame):
		floored_frame = int(math.floor(scene.frame_start+frame))
		subframe = frame - floored_frame
		scene.frame_set(floored_frame, subframe=subframe)

	armatures = []
	for obj in scene.objects:
		if ( not only_selected ) or ( obj in bpy.context.selected_objects ):
			debug_print("Object:", obj.name)
			debug_print("\tPosition Type:", position_type)
			debug_print("\tRotation Type:", rotation_type)
			relevant_object = diesel_animation.add_object(obj.name)

			# Position setup
			positions_dict = {}
			def add_position(frame, default_frame):
				debug_print("\t\tPosition Frame", frame)
				set_frame(frame)

				trans = obj.matrix_local.translation
				relevant_object.add_position(
					frame/framerate,
					trans.x * 100 * blender_scale,
					trans.y * 100 * blender_scale,
					trans.z * 100 * blender_scale,
					default_frame
				)

			# Rotation Setup
			rotations_dict = {}
			def add_rotation(frame, default_frame):
				debug_print("\t\tRotation Frame", frame)
				set_frame(frame)

				quat = obj.matrix_local.to_quaternion()
				relevant_object.add_rotation(
					frame/framerate,
					quat.x,
					quat.y,
					quat.z,
					quat.w,
					default_frame
				)

			if obj.animation_data and obj.animation_data.action and obj.animation_data.action.fcurves:
				for fcurve in obj.animation_data.action.fcurves:
					for keyframe in fcurve.keyframe_points:

						if fcurve.data_path == "location":
							frame = keyframe.co[0]
							if not frame in positions_dict:
								add_position(frame, False)
								positions_dict[frame] = True

						elif fcurve.data_path == "rotation_quaternion" or fcurve.data_path == "rotation_axis_angle" or fcurve.data_path == "rotation_euler":
							frame = keyframe.co[0]
							if not frame in rotations_dict:
								add_rotation(frame, False)
								rotations_dict[frame] = True

			do_detailed_positions = False
			do_detailed_rotations = False
			if export_detailed and (len(relevant_object.positions) > 1):
				do_detailed_positions = True
			if export_detailed and (len(relevant_object.rotations) > 1):
				do_detailed_rotations = True

			for frame in range(scene.frame_start, scene.frame_end + 1):
				if do_detailed_positions:
					add_position(frame, False)
				if do_detailed_rotations:
					add_rotation(frame, False)

			if len(relevant_object.positions) == 0:
				debug_print("\tDefault Frame")
				add_position(0, True)

			if len(relevant_object.rotations) == 0:
				debug_print("\tDefault Frame")
				add_rotation(0, True)

			if obj.pose and obj.pose.bones:
				armatures.append(obj)

	# Bone Time
	for armature in armatures:
		for bone in armature.pose.bones:
			debug_print("Bone:", bone.name)
			debug_print("\tPosition Type:", position_type)
			debug_print("\tRotation Type:", rotation_type)
			relevant_object = diesel_animation.add_object(bone.name)

			# Position setup
			positions_dict = {}
			def add_position(frame, default_frame):
				debug_print("\t\tPosition Frame", frame)
				set_frame(frame)

				parent_relative_matrix = bone.matrix
				if bone.parent:
					parent_relative_matrix = helpers.matrix_mult(bone.parent.matrix.inverted(), bone.matrix)

				trans = parent_relative_matrix.translation
				relevant_object.add_position(
					frame/framerate,
					trans.x * 100 * blender_scale,
					trans.y * 100 * blender_scale,
					trans.z * 100 * blender_scale,
					default_frame
				)

			# Rotation Setup
			rotations_dict = {}
			def add_rotation(frame, default_frame):
				debug_print("\t\tRotation Frame", frame)
				set_frame(frame)

				parent_relative_matrix = bone.matrix
				if bone.parent:
					parent_relative_matrix = helpers.matrix_mult(bone.parent.matrix.inverted(), bone.matrix)

				quat = parent_relative_matrix.to_quaternion()
				relevant_object.add_rotation(
					frame/framerate,
					quat.x,
					quat.y,
					quat.z,
					quat.w,
					default_frame
				)

			if armature.animation_data and armature.animation_data.action and armature.animation_data.action.fcurves:
				for fcurve in armature.animation_data.action.fcurves:
					for keyframe in fcurve.keyframe_points:

						if fcurve.data_path == "pose.bones[\"" + bone.name + "\"].location":
							frame = keyframe.co[0]
							if not frame in positions_dict:
								add_position(frame, False)
								positions_dict[frame] = True

						elif fcurve.data_path == "pose.bones[\"" + bone.name + "\"].rotation_quaternion" or fcurve.data_path == "pose.bones[\"" + bone.name + "\"].rotation_axis_angle" or fcurve.data_path == "pose.bones[\"" + bone.name + "\"].rotation_euler":
							frame = keyframe.co[0]
							if not frame in rotations_dict:
								add_rotation(frame, False)
								rotations_dict[frame] = True

			do_detailed_positions = False
			do_detailed_rotations = False
			if export_detailed and (len(relevant_object.positions) > 1):
				do_detailed_positions = True
			if export_detailed and (len(relevant_object.rotations) > 1):
				do_detailed_rotations = True

			for frame in range(scene.frame_start, scene.frame_end + 1):
				if do_detailed_positions:
					add_position(frame, False)
				if do_detailed_rotations:
					add_rotation(frame, False)

			if len(relevant_object.positions) == 0:
				debug_print("\t\tDefault Frame")
				add_position(0, True)

			if len(relevant_object.rotations) == 0:
				debug_print("\t\tDefault Frame")
				add_rotation(0, True)

	############
	# TRIGGERS #
	############

	triggers = []

	if export_triggers:
		for marker in scene.timeline_markers:
			diesel_animation.add_trigger(
				(marker.frame-scene.frame_start)/framerate,
				marker.name
			)

	##########
	# OUTPUT #
	##########

	diesel_animation.write(filepath, export_64bit, position_type, rotation_type)

	if debug_output:
		debug_prints_enabled = False
		animation_format.debug_prints_enabled = False