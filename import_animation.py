debug_prints_enabled = False
def debug_print(*args):
	if debug_prints_enabled:
		print(*args)

from . import helpers, animation_format

import bpy
from bpy.props import (IntProperty, StringProperty, BoolProperty)
from mathutils import (Vector, Quaternion, Matrix, Euler)
from bpy_extras.io_utils import (ImportHelper)

import zlib
import struct

class DieselAnimationImporter(bpy.types.Operator, ImportHelper):
	bl_idname = "import_scene.animation"
	bl_label = "Import Diesel Animation"
	bl_description = "Import a Diesel Animation"
	bl_options = {'UNDO'}

	filename_ext = ".animation"
	filter_glob = StringProperty(default = "*.animation", options = {'HIDDEN'})

	import_triggers = BoolProperty(
		name="Import Triggers",
		default=True
	)

	import_positions = BoolProperty(
		name="Import Positions",
		default=True
	)

	import_rotations = BoolProperty(
		name="Import Rotations",
		default=True
	)

	framerate = IntProperty(
		name="Frame Rate",
		description="The frame rate to match timings to.",
		default=30,
		min=0,
		max=2400
	)

	only_selected = BoolProperty(
		name="Only Apply to Selected",
		default=False
	)

	def execute(self, context):
		animation_import(self.filepath,
			self.properties.import_triggers,
			self.properties.import_positions,
			self.properties.import_rotations,
			self.properties.framerate,
			self.properties.only_selected,
			helpers.get_property("importDebugOutput")
		)

		return {'FINISHED'}

	def invoke(self, context, event):
		bpy.context.window_manager.fileselect_add(self)

		return {'RUNNING_MODAL'}

def animation_import(filepath, import_triggers=True, import_positions=True, import_rotations=True, framerate=30, only_selected=False, debug_output=False):
	if debug_output:
		debug_prints_enabled = True
		animation_format.debug_prints_enabled = True

	diesel_animation = animation_format.DieselAnimation()
	diesel_animation.read(filepath)

	################
	# BLENDER TIME #
	################
	debug_print("\nBlender Time:")

	# Generic Scene Setup
	scene = bpy.context.scene

	scene.frame_current = scene.frame_start
	scene.frame_end = scene.frame_start + round(diesel_animation.length * framerate)

	blender_scale = scene.unit_settings.scale_length

	# Gather all the objects and bones that exist in the scene and the animation.
	objects = {}
	bones = {}

	caps_object_names = []
	for diesel_object in diesel_animation.objects:
		caps_object_names.append(diesel_object.name.upper())

	def recurse_bones(obj, bone):
		bone_name = bone.name.upper()
		if bone_name in caps_object_names:
			bones[bone_name] = (obj,bone)

		for child in bone.children:
			recurse_bones(obj, child)

	def recurse(obj):
		if ( not only_selected ) or ( obj in bpy.context.selected_objects ):
			obj_name = obj.name.upper()
			if obj_name in caps_object_names:
				objects[obj_name] = obj

			if obj.pose and obj.pose.bones:
				for bone in obj.pose.bones:
					if bone.parent == None:
						recurse_bones(obj, bone)

		for child in obj.children:
			recurse(child)

	for obj in scene.objects:
		if obj.parent == None:
			recurse(obj)

	# Keyframe Time
	for diesel_object in diesel_animation.objects:
		if diesel_object.name.upper() in objects.keys():
			debug_print("\tFound Object: {}".format(diesel_object.name))

			potential_object = objects[diesel_object.name.upper()]

			if import_positions:
				debug_print("\t\tImporting Positions:")

				for time, diesel_frame in sorted(diesel_object.positions.items()):
					if not diesel_frame.stepped:
						position = Vector(diesel_frame.data)
						frame = time * framerate

						parent_relative_matrix = potential_object.matrix_local
						frame_matrix = parent_relative_matrix.to_quaternion().to_matrix().to_4x4()
						frame_matrix.translation = (position / 100) / blender_scale

						potential_object.matrix_local = frame_matrix
						potential_object.keyframe_insert(data_path='location', frame=scene.frame_start+frame)

						debug_print("\t\t\t{:<24} - {}".format(frame,potential_object.location))

			if import_rotations:
				debug_print("\t\tImporting Rotations:")

				for time, diesel_frame in sorted(diesel_object.rotations.items()):
					if not diesel_frame.stepped:
						rotation = Quaternion((
							diesel_frame.data[3],
							diesel_frame.data[0],
							diesel_frame.data[1],
							diesel_frame.data[2]
						))
						frame = time * framerate

						potential_object.rotation_mode = 'QUATERNION'

						parent_relative_matrix = potential_object.matrix_local
						frame_matrix = rotation.to_matrix().to_4x4()
						frame_matrix.translation = parent_relative_matrix.translation

						potential_object.matrix_local = frame_matrix
						potential_object.keyframe_insert(data_path='rotation_quaternion', frame=scene.frame_start+frame)

						debug_print("\t\t\t{:<24} - {}".format(frame,potential_object.rotation_quaternion))

		elif diesel_object.name.upper() in bones.keys():
			debug_print("\tFound Bone: {}".format(diesel_object.name))

			potential_bone = bones[diesel_object.name.upper()][1]
			armature = bones[diesel_object.name.upper()][0]

			if import_positions:
				debug_print("\t\tImporting Positions:")

				for time, diesel_frame in sorted(diesel_object.positions.items()):
					if not diesel_frame.stepped:
						position = Vector(diesel_frame.data)
						frame = time * framerate

						parent_relative_matrix = potential_bone.matrix
						if potential_bone.parent:
							parent_relative_matrix = armature.convert_space(pose_bone=potential_bone.parent, matrix=potential_bone.matrix, from_space="POSE", to_space="LOCAL")

						frame_matrix = parent_relative_matrix.to_quaternion().to_matrix().to_4x4()
						frame_matrix.translation = (position / 100) / blender_scale
						if potential_bone.parent:
							frame_matrix = armature.convert_space(pose_bone=potential_bone.parent, matrix=frame_matrix, from_space="LOCAL", to_space="POSE")

						potential_bone.matrix = frame_matrix
						potential_bone.keyframe_insert(data_path='location', frame=scene.frame_start+frame)

						debug_print("\t\t\t{:<24} - {}".format(frame,potential_bone.location))

			if import_rotations:
				debug_print("\t\tImporting Rotations:")

				for time, diesel_frame in sorted(diesel_object.rotations.items()):
					if not diesel_frame.stepped:
						rotation = Quaternion((
							diesel_frame.data[3],
							diesel_frame.data[0],
							diesel_frame.data[1],
							diesel_frame.data[2]
						))
						frame = time * framerate

						parent_relative_matrix = potential_bone.matrix
						if potential_bone.parent:
							parent_relative_matrix = armature.convert_space(pose_bone=potential_bone.parent, matrix=potential_bone.matrix, from_space="POSE", to_space="LOCAL")

						frame_matrix = rotation.to_matrix().to_4x4()
						frame_matrix.translation = parent_relative_matrix.translation
						if potential_bone.parent:
							frame_matrix = armature.convert_space(pose_bone=potential_bone.parent, matrix=frame_matrix, from_space="LOCAL", to_space="POSE")

						potential_bone.matrix = frame_matrix
						potential_bone.rotation_mode = 'QUATERNION'
						potential_bone.keyframe_insert(data_path='rotation_quaternion', frame=scene.frame_start+frame)

						debug_print("\t\t\t{:<24} - {}".format(frame,potential_bone.rotation_quaternion))

		else:
			debug_print("\tFailed to Locate: {}".format(diesel_object.name))

	# Triggers -> Markers
	if import_triggers:
		debug_print("\tImporting Triggers:")
		for trigger in diesel_animation.triggers:
			frame = round(trigger.time * framerate)
			scene.timeline_markers.new(trigger.name, frame=scene.frame_start+frame)

			debug_print("\t\t{:<4} - {}".format(frame,trigger.name))

	if debug_output:
		debug_prints_enabled = False
		animation_format.debug_prints_enabled = False