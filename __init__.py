﻿#  Copyright (c) 2021 Connie Price contact@connieprice.co.uk
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# Plugin Info
bl_info = {
	"name": "Diesel Animation",
	"author": "Cpone",
	"blender": (2, 80, 0),
	"location": "File > Import-Export",
	"description": "Importer and exporter for Overkill's Diesel Engine animation format.",
	"category": "Import-Export"
}

# Reload Modules if Blender is Reloaded
if "bpy" in locals():
	import importlib
	if "preferences" in locals():
		importlib.reload(preferences)
	if "helpers" in locals():
		importlib.reload(helpers)
	if "animation_format" in locals():
		importlib.reload(animation_format)
	if "import_animation" in locals():
		importlib.reload(import_animation)
	if "export_animation" in locals():
		importlib.reload(export_animation)

import bpy
blast_to_the_past = (bpy.app.version < (2, 80))

# If we ain't in 2.8 then it's rewind time.
if blast_to_the_past:
	bl_info["blender"] = (2, 74, 0)

from . import preferences, import_animation, export_animation

# Basic Menu Functions
def menu_func_import(self, context):
	self.layout.operator(import_animation.DieselAnimationImporter.bl_idname, text="Diesel Animation (.animation)")

def menu_func_export(self, context):
	self.layout.operator(export_animation.DieselAnimationExporter.bl_idname, text="Diesel Animation (.animation)")

# Class Registration
classes = (
	preferences.DieselAnimationPreferences,
	import_animation.DieselAnimationImporter,
	export_animation.DieselAnimationExporter
)

def register():
	for cls in classes:
		helpers.make_annotations(cls)
		bpy.utils.register_class(cls)

	if blast_to_the_past:
		bpy.types.INFO_MT_file_import.append(menu_func_import)
		bpy.types.INFO_MT_file_export.append(menu_func_export)
	else:
		bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
		bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
	if blast_to_the_past:
		bpy.types.INFO_MT_file_import.remove(menu_func_import)
		bpy.types.INFO_MT_file_export.remove(menu_func_export)
	else:
		bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
		bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

	for cls in classes:
		bpy.utils.unregister_class(cls)